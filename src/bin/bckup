#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: A script to backup $HOME
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Code
# Last Modified: 2022-12-26
#------------------------------------------------------------------------------
# Dependencies:
#       bash
#       coreutils
#       git
#       zstd
#------------------------------------------------------------------------------

shopt -s extglob dotglob

declare -r prg=${0##*/}

die ()
{
	echo "$prg: $2" >&2
	(( $1 )) &&
		exit $1
}

(( $# )) &&
	die 1 "needn't arguments."

declare -i bin_err=0
for bin in cp git tar zstd; {
	if ! type -P "$bin" &>/dev/null; then
		die 0 "dependency, \`$bin\`, not met."
		(( bin_err++ ))
	fi
}

(( bin_err )) &&
	die 1 "$bin_err dependency(s) missing, aborted."
unset -v bin_err

# Add list of your backups destinations here (Must have '.tzst' suffix).
declare -ar dest=(
	"/run/media/$USER/Main Data/Home/$(printf '%(%F_%T)T').tzst"
)

for dir in "${dest[@]}"; {
	[[ -d ${dir%/*} ]] ||
		die 1 "${dir%/*}: not a directory."
}
unset -v dir

# I've to used `cd`, 'cause `tar` can't handle full path properly.
cd -- "$HOME"

declare -a src_list=()
for src in *; {
	case $src in
	.cache|.dbus|.nv|Desktop|Downloads)
		;;

	.config)
		# I've to exclude through 'Cache' directories, 'cause it's
		# takeup a lot of space.
		for dir in "$src"/*; {
			unset -v has_cache

			case $dir in
			dconf|pulse)
				continue ;;
			esac

			for cache in "$dir"/*Cache*; {
				[[ -d $cache ]] &&
					has_cache=true
				break
			}

			if [[ $has_cache != true ]]; then
				src_list+=("$dir")
				continue
			fi

			for non_cache in "$dir"/!(*Cache*); {
				src_list+=("$non_cache")
			}
		}
		unset -v dir cache has_cache non_cache ;;

	.local)
		for dir in "$src"/{bin,share,src}; {
			[[ -d $dir ]] &&
				src_list+=("$dir")
		}
		unset -v dir ;;

	*)
		src_list+=("$src") ;;
	esac
}
unset -v src

declare -ar src_list
tar cPf "$dest" -I 'zstd -1 -T0' "${src_list[@]}"

for src in "${dest[@]}"; {
	[[ $dest == $src ]] &&
		continue

	cp -a "$dest" "$src"
}

#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Create symlinks from this repository to $HOME
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Code
# Last Modified: 2022-05-29
#------------------------------------------------------------------------------

declare -r progrm=${0##*/}

die ()
{
	echo "$progrm: $2" >&2
	(( $1 )) &&
		exit $1
}

usage ()
{
	read -d '' <<-EOF
	Usage: $progrm [OPTIONS]

	  -h, --help        - Display this help infomation.
	  -c, --copy        - Copies instead of symlinks.

	EOF
	echo -n "$REPLY"
}

install ()
{
	ln -sv "$@"
}

while [[ -n $1 ]]; do
	case $1 in
	-h|--help)
		usage
		exit 0 ;;

	-c|--copy)
		install ()
		{
			cp -vnr "$@"
		} ;;

	*)
		die 1 "$1: invaild option..." ;;
	esac
	shift
done

base=$(realpath "$0")
declare -r base=${base%/*}/src

[[ -d $base ]] ||
	die 1 "$base: doesn't exist..."

[[ -d $HOME/Code ]] ||
	mkdir -p "$HOME"/Code

[[ -d $HOME/.local/bin ]] ||
	mkdir -p "$HOME"/.local/bin

for src in "$base"/{bin/,}*; {
	dest=$HOME/Code/${src#$base/}

	[[ $src == */bin ]] &&
		continue

	[[ $src == */bin/* ]] &&
		dest=$HOME/.local/bin/${src#$base/bin/}

	[[ -e $dest ]] && {
		die 0 "${dest/$HOME/\~}: already existed..."
		continue
	}

	find "$dest" -xtype l -delete &>/dev/null
	install "$src" "$dest"
}

exit 0
